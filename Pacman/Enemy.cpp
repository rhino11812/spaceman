#include "Enemy.h"



Enemy::Enemy(Vector2* pos, Rect* rect, Texture2D* tex) : Collidable(pos, rect, tex)
{
	direction = 0;
	speed = 0.2f;
	health = 3;
	dead = false;

	AI = still;
	currentFrametime = 0;
	frame = 0;
	frameTime = 300;

	
	
}


Enemy::~Enemy()
{
	delete texture;
	delete position;
	delete sourceRect;
}

void Enemy::UpdateGhosts(int elapsedTime, Player* pacman)
{
	float xDistance = pacman->getPosition()->X - position->X;
	float yDistance = pacman->getPosition()->Y - position->Y;
	float hypotenuse = sqrt((xDistance * xDistance) + (yDistance * yDistance));
	
	
	if (AI == still && hypotenuse < 300)
	{
		AI = chase;
		sourceRect->Y = 32;
	}
	else if (AI == chase && hypotenuse > 300)
	{
		AI = still; 
		sourceRect->Y = 0;
	}

	switch (AI)
	{
		case still:
			if (currentFrametime > frameTime)
			{
				frame++;
				if (frame >= 4)
					frame = 0;

				currentFrametime = 0;
			}
			sourceRect->X = sourceRect->Width * frame;
			currentFrametime += elapsedTime;
			break;

		case chase:	
			if (currentFrametime > frameTime)
			{
				frame++;
				if (frame >= 4)
					frame = 0;

				currentFrametime = 0;
			}
			sourceRect->X = sourceRect->Width * frame;
			currentFrametime += elapsedTime;
			position->X += elapsedTime * (xDistance / hypotenuse) / 5;
			position->Y += elapsedTime * (yDistance / hypotenuse) / 5;
			break;

		case patrol: 
			sourceRect->Y = sourceRect->Height * direction;
			if (direction == 1) //Moves down  
			{
				position->Y += speed * elapsedTime;
			}
			else if (direction == 0) //Moves up  
			{
				position->Y -= speed * elapsedTime;
			}
			if (position->Y + sourceRect->Width >= Graphics::GetViewportHeight() - 65) //Hits bottom
			{
				direction = 0; //Change direction  
			}
			else if (position->Y <= 0) //Hits top
			{
				direction = 1;//Change direction
			}
			break;
	}
}

void Enemy::SetAI(enemyAI ghostAI)
{
	AI = ghostAI;
}

enemyAI Enemy::GetAI()
{
	return AI;
}

void Enemy::DecrHealth()
{
	health--;
}

int Enemy::GetHealth()
{
	return health;
}

bool Enemy::CheckDead(Player* pacman)
{
	if (this->GetHealth() > 0)
	{
		return false;
	}
	else
		this->setPosition(-1000, -1000);


	if (this->GetHealth() > -1 && this->GetHealth() == 0)
	{
		pacman->SetXP(pacman->GetXP() + 50);
		health--;
	}

	return true;
	
}

void Enemy::setHealth(int x)
{
	health = x;
}


