#pragma once

#define GHOSTCOUNT 7
#define MUNCHIECOUNT 100
#define PORTALCOUNT 4

// If Windows and not in Debug, this will run without a console window
// You can use this to output information when debugging using cout or cerr
#ifdef WIN32 
	#ifndef _DEBUG
		#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
	#endif
#endif

// Just need to include main header file
#include "S2D/S2D.h"
#include "Collectible.h"
#include "Player.h"
#include "Enemy.h"
#include "Bullet.h"
#include "Portal.h"

// Reduces the amount of typing by including all classes in S2D namespace
using namespace S2D;

enum gameState
{
	mainMenu,
	controls,
	tutorial,
	level1,
	level2,
	level3,
	paused,
	deathScr,
	levelUp,
	nextLevel
};


class Pacman : public Game
{
private:
	gameState gState;
	gameState tempState;

	Collectible* munchie[MUNCHIECOUNT];
	Player* pacman;
	Enemy* ship[GHOSTCOUNT];
	Portal* portal[PORTALCOUNT];

	int ghostsToKill;


	// Data for sound
	SoundEffect* _pop;
	SoundEffect* levelOneSong;
	SoundEffect* levelTwoSong;
	SoundEffect* mainMenuSong;
	SoundEffect* deathSound;
	bool hasL1Played;
	bool hasL2Played;
	bool hasMenuPlayed;
	bool hasDeathPlayed;

	double shotTime;
	double maxShotTime;

	// Data for menus
	Texture2D* _menuSheet;
	Rect* _mainMenuRect;
	Vector2* _mainMenuPos;

	Texture2D* controlsScrTex;
	Rect* controlsScrRect;
	Vector2* controlsScrPos;

	// Position for Strings
	Vector2* _stringPosition;
	Vector2* _scoreStringPosition;
	Vector2* remainingPos;

	//Data for Menu/start screen
	Texture2D* logoTex;
	Rect* logoRect;
	Vector2* logoPos;
	Texture2D* menuButtons;
	Rect* startRect;
	Vector2* startPos;
	Rect* controlsRect;
	Vector2* controlsPos;
	Rect* exitRect;
	Vector2* exitPos;

	int score;

	Texture2D* _menuBackground;
	Rect* _menuRectangle;
	Vector2* _menuStringPosition;
	bool _paused;
	bool _started; 
	bool _pKeyDown;
	bool _spaceKeyDown;

	//Data for bakcground
	Texture2D* _backGround1;
	Rect* _backGroundRect1;
	Vector2* _backGroundPos1;

	Texture2D* nextLevelTex;
	Rect* nextLevelRect;
	Vector2* nextLevelPos;


	// Data for levelup
	Texture2D* levelUpTex;
	Rect* levelUpRect;
	Vector2* levelUpPos;
	Texture2D* _levelUpO1;
	Rect* _levelUpO1Rect;
	Vector2* _levelUpO1Pos;
	Texture2D* XPBoostLevelTex;
	Rect* XPBoostLevelRect;
	Vector2* XPBoostLevelPos;
	Texture2D* boostTimeTex; 
	Rect* boostTimeRect;
	Vector2* boostTimePos;

	Vector2* _levelStringPos;


	// Data for death
	Texture2D* _restartScreen;
	Rect* _restartScreenRect;
	Vector2* _restartScreenPos;
	bool _restartScreenOn;
	bool _rKeyDown;

	//Data for xp bar
	Vector2* _xpBarStringPos;

	void Input(int elapsedTime, Input::KeyboardState* state, Input::MouseState* mouseState);

	void CheckPaused(Input::KeyboardState* state, Input::Keys pauseKey);
	void CheckRestart(Input::KeyboardState* state);
	
	bool CheckCollisions(Collidable& objA, Collidable& objB);
	bool Checkhover(Vector2 pos, Rect rec);
	bool CheckClick(Vector2 pos, Rect rec);

	bool loaded;
	bool hasLevelOneLoaded;
	bool hasLevelTwoLoaded;
	bool hasLevelThreeLoaded;


	void Restart();

	//Levels
	void mainMenuUpdate();
	void mainMenuDraw();

	void controlsUpdate();
	void controlsDraw();

	void LevelOneLoad();
	void levelOneUpdate(int elapsedTime);
	void levelOneDraw();

	void levelTwoLoad();
	void levelTwoUpdate(int elapsedTime);
	void levelTwoDraw();

	void levelThreeLoad();
	void levelThreeUpdate(int elapsedTime);
	void levelThreeDraw();

	void pauseUpdate();
	void pauseDraw();

	void LevelUpUpdate(int elapsedTime);
	void levelUpDraw();

	void deathUpdate();
	void deathDraw();

	void nextLevelUpdate();
	void nextLevelDraw();

	void ResetLoaded();


	
public:
	/// <summary> Constructs the Pacman class. </summary>
	Pacman(int argc, char* argv[]);

	/// <summary> Destroys any data associated with Pacman class. </summary>
	virtual ~Pacman();

	/// <summary> All content should be loaded in this method. </summary>
	void virtual LoadContent();

	/// <summary> Called every frame - update game logic here. </summary>
	void virtual Update(int elapsedTime);

	/// <summary> Called every frame - draw game here. </summary>
	void virtual Draw(int elapsedTime);
};