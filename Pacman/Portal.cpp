#include "Portal.h"


Portal::Portal(Vector2* pos, Rect* rect, Texture2D* tex) : Collidable(pos, rect, tex)
{
	currentFrametime = 0;
	frame = 0;
	frameTime = 300;
}


Portal::~Portal()
{
	delete texture;
	delete position;
	delete sourceRect;
}

void Portal::Update(int elapsedTime)
{
	if (currentFrametime > frameTime)
	{
		frame++;
		if (frame >= 4)
			frame = 0;

		currentFrametime = 0;
	}
	sourceRect->X = sourceRect->Width * frame;
	currentFrametime += elapsedTime;
}