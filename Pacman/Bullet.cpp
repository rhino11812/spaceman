#include "Bullet.h"


Bullet::Bullet(Vector2* pos, Rect* rect, Texture2D* tex, Vector2* targetPosi, Vector2* startPos) : Collidable(pos, rect, tex)
{
	currentTime = 0;
	maxTime = 1;
	bool shooting = false;
	targetPos = targetPosi;
	startPosition = startPos;
	xDistance = (targetPos->X - startPosition->X) / 3;
	yDistance = (targetPos->Y - startPosition->Y) / 3;
}

Bullet::~Bullet()
{
}

void Bullet::update(int elapsedTime)
{
	float hypotenuse = sqrt((xDistance * xDistance) + (yDistance * yDistance));
	position->X += elapsedTime * (xDistance / hypotenuse);
	position->Y += elapsedTime * (yDistance / hypotenuse);
}

bool Bullet::CheckViewPortColl()
{
	if (position->X > Graphics::GetViewportWidth() - sourceRect->Width)
		return true;
	if (position->X < 0)
		return true;
	if (position->Y >(Graphics::GetViewportHeight() - sourceRect->Height) - 65)
		return true;
	if (position->Y < 0())
		return true;

	return false;
}