#pragma once


#include "Collidable.h"
#include "Bullet.h"
#include <vector>

using namespace std;


class Player : public Collidable
{
public:
	vector<Bullet> bulletList;

	Player(Vector2* pos, Rect* rect, Texture2D* tex, Texture2D* bulletText);
	~Player();

	void Update(int elapsedTime);

	void CheckPacmanInput(int elapsedTime, Input::KeyboardState* state, Input::MouseState* mState);
	void CheckViewportCollision();

	float GetMaxBoost();
	float GetActualBoost();

	bool CheckRight();
	bool CheckLeft();

	float GetActualSpeed();
	float GetSpeed();
	float GetTempSpeed();
	void SetActualSpeed(float x);
	void SetSpeed(float x);
	void SetTempSpeed(float x);

	int GetXP();
	void SetXP(int xpInc);
	int GetXPInc();
	void SetXPInc(int x);
	int GetLevel();
	void SetLevel(int x);

	void SetDead(bool x);
	bool GetDead();

	void shoot(Input::MouseState*state);
	void deleteBullet(int i);

	int getSpeedLevel();
	void setSpeedLevel(int x);
	int getXPBoostLevel();
	void setXPBoostLevel(int x);
	int getBoostTimeLevel();
	void setBoostTimeLevel(int x);

	int getBoostTime();
	void setBoostTime(int x);




	
private: 

		int direction;
		int frametime;
		int frame;
		int currentFrameTime;
		float speed;
		float tempSpeed;
		float actualSpeed;
		bool dead;
		bool stopSpeedRight;
		bool stopSpeedLeft;

		double shotTime;
		double maxShotTime;

		//Data for boost
		float boostTime;
		float maxBoostTime;
		bool isBoosting;
		float boostSpeed;
		bool isShiftKeyDown;


		bool wDown;
		bool aDown;
		bool sDown;
		bool dDown;

		int xp;
		int xpInc;
		int score;
		int level;

		int speedLevel;
		int XPBoostLevel;
		int boostTimeLevel;

		int boostTimeSpeed;

		//bullet data
		Texture2D* bulletTex;
		SoundEffect* _gun;
		SoundEffect* _collision;
		
};

