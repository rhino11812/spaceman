#include "Pacman.h"


#include <sstream>
#include <iostream>
#include <fstream>
#include <time.h>

Pacman::Pacman(int argc, char* argv[]) : Game(argc, argv)
{
	srand(time(NULL));

	gState = mainMenu;
	_paused = false;
	_pKeyDown = false;
	_spaceKeyDown = false;
	_started = false;

	score = 0;

	//Bools for music
	hasL1Played = false;
	hasL2Played = false;
	hasMenuPlayed = false;
	hasDeathPlayed = false;


	//bools for loading
	bool loaded = false;
	hasLevelOneLoaded = false;
	hasLevelTwoLoaded = false; 
	hasLevelThreeLoaded = false;

	shotTime = 0.0;
	maxShotTime = 0.3;

	//Data for death
	_restartScreenOn = false;
	_rKeyDown = false;

	//Initialise important Game aspects
	Audio::Initialise();
	Graphics::Initialise(argc, argv, this, 1024, 768, false, 25, 25, "Pacman", 60);
	Input::Initialise();

	// Start the Game Loop - This calls Update and Draw in game loop
	Graphics::StartGameLoop();
}

Pacman::~Pacman()
{
	delete pacman;
	delete _levelUpO1;
	delete _levelUpO1Pos;
	delete _levelUpO1Rect;
	delete _backGround1;
	delete _backGroundRect1;
	delete _backGroundPos1;
	delete _stringPosition;
	delete _xpBarStringPos;
	delete _levelStringPos;
	delete _menuBackground;
	delete _menuRectangle;
	delete munchie;
	delete levelOneSong;
}

void Pacman::LoadContent()
{

#pragma region Core content
	if (!loaded)
	{


		_pop = new SoundEffect();
		_pop->Load("Sounds/levelUp.wav");

		levelOneSong = new SoundEffect(true, 1, 1);
		levelOneSong->Load("Sounds/The Lift.wav");

		levelTwoSong = new SoundEffect(true, 1, 1);
		levelTwoSong->Load("Sounds/level2.wav");

		mainMenuSong = new SoundEffect(true, 1, 1);
		mainMenuSong->Load("Sounds/Mighty Like Us.wav");

		deathSound = new SoundEffect();
		deathSound->Load("Sounds/deathSound.wav");

		// Load menusheet
		_menuSheet = new Texture2D();
		_menuSheet->Load("Textures/Backgrounds/MenuSheet.jpg", true);
		_mainMenuRect = new Rect(0.0f, 0.0f, Graphics::GetViewportWidth(), Graphics::GetViewportHeight());
		_mainMenuPos = new Vector2(0.0f, 0.0f);
		logoTex = new Texture2D();
		logoTex->Load("Textures/logo.png", false);
		logoRect = new Rect(0, 0, 450, 90);
		logoPos = new Vector2((Graphics::GetViewportWidth() / 2) - 225, 30);

		nextLevelTex = new Texture2D();
		nextLevelTex->Load("Textures/Backgrounds/nextLevelScreen.png", false);
		nextLevelRect = new Rect(0.0f, 0.0f, Graphics::GetViewportWidth(), Graphics::GetViewportHeight());
		nextLevelPos = new Vector2(0.0f, 0.0f);

		controlsScrTex = new Texture2D();
		controlsScrTex->Load("Textures/Backgrounds/controls.png", false);
		controlsScrRect = new Rect(0.0f, 0.0f, Graphics::GetViewportWidth(), Graphics::GetViewportHeight());
		controlsScrPos = new Vector2(0.0f, 0.0f);

		//Load menuButtons
		menuButtons = new Texture2D();
		menuButtons->Load("Textures/UI/StartBut.png", false);

		startRect = new Rect(0.0f, 0.0f, 200, 50);
		startPos = new Vector2((Graphics::GetViewportWidth() / 2) - 100, 250);

		controlsRect = new Rect(0.0f, 50, 200, 50);
		controlsPos = new Vector2((Graphics::GetViewportWidth() / 2) - 100, 310);

		exitRect = new Rect(0.0f, 100, 200, 50);
		exitPos = new Vector2((Graphics::GetViewportWidth() / 2) - 100, 370);

		levelUpTex = new Texture2D();
		levelUpTex->Load("Textures/UI/levelup.png", false);
		levelUpRect = new Rect(0, 0, 300, 100);
		levelUpPos = new Vector2((Graphics::GetViewportWidth() / 2) - 150, 100);
		_levelUpO1 = new Texture2D();
		_levelUpO1->Load("Textures/UI/speedboost.png", false);
		_levelUpO1Pos = new Vector2((Graphics::GetViewportWidth() / 2) - 100, 300);
		_levelUpO1Rect = new Rect(0, 0, 200, 50);
		XPBoostLevelTex = new Texture2D();
		XPBoostLevelTex->Load("Textures/UI/xpBoost.png", false);
		XPBoostLevelPos = new Vector2((Graphics::GetViewportWidth() / 2) - 100, 360);
		XPBoostLevelRect = new Rect(0, 0, 200, 50);
		boostTimeTex = new Texture2D();
		boostTimeTex->Load("Textures/UI/boostTime.png", false);
		boostTimeRect = new Rect(0, 0, 200, 50);
		boostTimePos = new Vector2((Graphics::GetViewportWidth() / 2) - 100, 420);


		//Set deathsreen
		_restartScreen = new Texture2D();
		_restartScreen->Load("Textures/Backgrounds/RestartScreen.png", false);
		_restartScreenRect = new Rect(0.0f, 0.0f, Graphics::GetViewportWidth(), Graphics::GetViewportHeight());
		_restartScreenPos = new Vector2(0.0f, 0.0f);

		// Set string positions
		_stringPosition = new Vector2(10.0f, 725.0f);
		_xpBarStringPos = new Vector2(350, 747);
		_levelStringPos = new Vector2(250, 725);
		remainingPos = new Vector2(470, 747);

		//Set menu parameters
		_menuBackground = new Texture2D();
		_menuBackground->Load("Textures/Backgrounds/PauseMenu.png", false);
		_menuRectangle = new Rect(0.0f, 0.0f, Graphics::GetViewportWidth(), Graphics::GetViewportHeight());

		loaded = true;
	}
#pragma endregion

#pragma region load level specifics
	switch (gState)
	{
	case tutorial:
		break;

	case level1:
		if (!hasLevelOneLoaded)
		{
			LevelOneLoad();
		}
		break;

	case level2:
		if (!hasLevelTwoLoaded)
		{
			levelTwoLoad();
		}
		break;

	case level3:
		if (!hasLevelThreeLoaded)
		{
			levelThreeLoad();
		}
		break;
	}

#pragma endregion

}

void Pacman::Update(int elapsedTime)
{
	switch (gState)
	{
		case mainMenu:
			mainMenuUpdate();
			break;

		case controls: 
			controlsUpdate();
			break;

		case tutorial:
			break;

		case level1:
			if (hasLevelOneLoaded)
			{
				levelOneUpdate(elapsedTime);
			}
			break;

		case level2:
			if (hasLevelTwoLoaded)
			{
				levelTwoUpdate(elapsedTime);
			}
			break;

		case level3: 
			if (hasLevelThreeLoaded)
			{
				levelThreeUpdate(elapsedTime);
			}
			break;

		case paused:
			pauseUpdate();
			break;

		case deathScr:
			deathUpdate();
			break;

		case levelUp:
			LevelUpUpdate(elapsedTime);
			break;
		case nextLevel:
			nextLevelUpdate();
	}
}

void Pacman::Draw(int elapsedTime)
{
	SpriteBatch::BeginDraw();
	switch (gState)
	{
		case mainMenu:
			mainMenuDraw();
			break;

		case controls:
			controlsDraw();
			break;

		case tutorial:
			break;

		case level1:
			if (hasLevelOneLoaded)
				levelOneDraw();
			break;

		case level2:
			if (hasLevelTwoLoaded)
				levelTwoDraw();
			break;

		case level3:
			if (hasLevelThreeLoaded)
				levelThreeDraw();
			break;

		case paused:
			switch (tempState)
			{
				case tutorial:
					break;

				case level1: 
					levelOneDraw();
					break;

				case level2: 
					levelTwoDraw();
					break;

				case level3:
					levelThreeDraw();
					break;
			}

			pauseDraw();
			break;

		case deathScr:
			deathDraw();
			break;

		case levelUp:
			switch (tempState)
			{
				case level1:
					levelOneDraw();
					break;

				case level2:
					levelTwoDraw();
					break;

				case level3:
					levelThreeDraw();
					break;
			}
		levelUpDraw();
		break;

		case nextLevel:
			nextLevelDraw();

	}
	SpriteBatch::EndDraw(); // Ends Drawing
}

void Pacman::Input(int elapsedTime, Input::KeyboardState* state, Input::MouseState* mouseState)
{
	pacman->CheckPacmanInput(elapsedTime, state, mouseState);
}

void Pacman::CheckPaused(Input::KeyboardState* state, Input::Keys pauseKey)
{
	if (state->IsKeyDown(Input::Keys::P) && !_pKeyDown)
	{
		_pKeyDown = true;
		if (gState == paused)
		{
			gState = tempState;
		}
		else
		{
			tempState = gState;
			gState = paused;
		}
	}

	if (state->IsKeyUp(Input::Keys::P))
	{
		_pKeyDown = false;
	}
}

void Pacman::CheckRestart(Input::KeyboardState* state)
{
	if (state->IsKeyDown(Input::Keys::R) && !_rKeyDown)
	{
		_rKeyDown = true;
		Restart();
	}
	if (state->IsKeyUp(Input::Keys::R))
	{
		_rKeyDown = false;
	}
}

void Pacman::Restart()
{
	gState = mainMenu;
	ResetLoaded();
	hasDeathPlayed = false;
}

bool Pacman::CheckCollisions(Collidable& objA, Collidable& objB)
{
	int x1 = objA.getPosition()->X;
	int x2 = objB.getPosition()->X;
	int y1 = objA.getPosition()->Y;
	int y2 = objB.getPosition()->Y;
	int X1 = x1 + objA.getRect()->Width;
	int X2 = x2 + objB.getRect()->Width;
	int Y1 = y1 + objA.getRect()->Height;
	int Y2 = y2 + objB.getRect()->Height;

	if (Y1 < y2)
		return false;
	if (y1 > Y2)
		return false;
	if (X1 < x2)
		return false;
	if (x1 > X2)
		return false;

	return true;
}

void Pacman::pauseUpdate()
{
	Input::KeyboardState* keyboardState = Input::Keyboard::GetState();

	CheckPaused(keyboardState, Input::Keys::P);
}

void Pacman::pauseDraw()
{
	SpriteBatch::Draw(_menuBackground, _menuRectangle, nullptr);
}

void Pacman::mainMenuUpdate()
{
	if (!hasMenuPlayed)
	{
		Audio::Play(mainMenuSong);
		hasMenuPlayed = true;
	}

#pragma region Button checks
	if (Checkhover(*startPos, *startRect))
	{
		startRect->X = startRect->Width;
	}
	else
	{
		startRect->X = 0;
	}
	if (Checkhover(*controlsPos, *controlsRect))
	{
		controlsRect->X = controlsRect->Width;
	}
	else
	{
		controlsRect->X = 0;
	}
	if (Checkhover(*exitPos, *exitRect))
	{
		exitRect->X = exitRect->Width;
	}
	else
	{
		exitRect->X = 0;
	}
	if (CheckClick(*startPos, *startRect))
	{
		Audio::Stop(mainMenuSong);
		hasMenuPlayed = false;
		_spaceKeyDown = true;
		gState = level1;
		LoadContent();
	}
	if (CheckClick(*exitPos, *exitRect))
	{
		Audio::Destroy();
		Graphics::Destroy();
		Input::Destroy();
	}
	if (CheckClick(*controlsPos, *controlsRect))
	{
		gState = controls;
	}
#pragma endregion
}

void Pacman::mainMenuDraw()
{
	SpriteBatch::Draw(_menuSheet, _mainMenuPos, _mainMenuRect, Vector2::Zero, 1.0f, 0.0f, Color::White, SpriteEffect::NONE);
	SpriteBatch::Draw(menuButtons, startPos, startRect, Vector2::Zero, 1.0f, 0.0f, Color::White, SpriteEffect::NONE);
	SpriteBatch::Draw(menuButtons, exitPos, exitRect, Vector2::Zero, 1.0f, 0.0f, Color::White, SpriteEffect::NONE);
	SpriteBatch::Draw(menuButtons, controlsPos, controlsRect, Vector2::Zero, 1.0f, 0.0f, Color::White, SpriteEffect::NONE);
	SpriteBatch::Draw(logoTex, logoPos, logoRect, Vector2::Zero, 1.0f, 0.0f, Color::White, SpriteEffect::NONE);

}

void Pacman::LevelUpUpdate(int elapsedTime)
{
	#pragma region Check speed level up
	if (Checkhover(*_levelUpO1Pos, *_levelUpO1Rect))
	{
		_levelUpO1Rect->X = 200;
	}
	else
	{
		_levelUpO1Rect->X = 0;
	}

	if (CheckClick(*_levelUpO1Pos, *_levelUpO1Rect) && pacman->getSpeedLevel() <= 2)
	{
		_levelUpO1Rect->Y += 50;
		pacman->SetXP(0);
		pacman->SetActualSpeed(pacman->GetActualSpeed() * 2);
		pacman->SetSpeed(pacman->GetSpeed() * 1.5);
		pacman->SetTempSpeed(pacman->GetTempSpeed() * 1.5);
		if (pacman->GetXPInc() > 2)
		pacman->SetXPInc(pacman->GetXPInc() - 2);
		pacman->SetLevel(pacman->GetLevel() + 1);
		pacman->setSpeedLevel(pacman->getSpeedLevel() + 1);
		gState = tempState;
	}
	#pragma endregion
	#pragma region Check xpboost level up
	if (Checkhover(*XPBoostLevelPos, *XPBoostLevelRect))
	{
		XPBoostLevelRect->X = 200;
	}
	else
	{
		XPBoostLevelRect->X = 0;
	}

	if (CheckClick(*XPBoostLevelPos, *XPBoostLevelRect) && pacman->getXPBoostLevel() <= 2)
	{
		XPBoostLevelRect->Y += 50;
		pacman->SetXP(0);
		pacman->SetXPInc(pacman->GetXPInc() + 2);
		pacman->SetLevel(pacman->GetLevel() + 1);
		pacman->setXPBoostLevel(pacman->getXPBoostLevel() + 1);
		gState = tempState;
	}
#pragma endregion
	#pragma region Check boosttime level up
	if (Checkhover(*boostTimePos, *boostTimeRect))
	{
		boostTimeRect->X = 200;
	}
	else
	{
		boostTimeRect->X = 0;
	}

	if (CheckClick(*boostTimePos, *boostTimeRect) && pacman->getBoostTimeLevel() <= 2)
	{
		boostTimeRect->Y += 50;
		pacman->SetXP(0);
		if (pacman->GetXPInc() > 2)
		pacman->SetXPInc(pacman->GetXPInc() - 2);
		pacman->SetLevel(pacman->GetLevel() + 1);
		pacman->setBoostTimeLevel(pacman->getBoostTimeLevel() + 1);
		pacman->setBoostTime(pacman->getBoostTime() - 500);
		gState = tempState;
	}
#pragma endregion
}

bool Pacman::Checkhover(Vector2 pos, Rect rec)
{
	Input::MouseState* mouseState = Input::Mouse::GetState();
	int mouseX;
	int mouseY;
	mouseX = mouseState->X;
	mouseY = mouseState->Y;
	if (mouseX > pos.X && mouseX < pos.X + rec.Width && mouseY > pos.Y && mouseY < pos.Y + rec.Height)
		return true;

	return false;
}

bool Pacman::CheckClick(Vector2 pos, Rect rec)
{
	Input::MouseState* mouseState = Input::Mouse::GetState();
	if (mouseState->LeftButton == Input::ButtonState::PRESSED)
	{
		int mouseX;
		int mouseY;
		mouseX = mouseState->X;
		mouseY = mouseState->Y;
		if (mouseX > pos.X && mouseX < pos.X + rec.Width && mouseY > pos.Y && mouseY < pos.Y + rec.Height)
			return true;
	}
	return false;
}

void Pacman::levelUpDraw()
{
	SpriteBatch::Draw(levelUpTex, levelUpPos, levelUpRect);
	SpriteBatch::Draw(_levelUpO1, _levelUpO1Pos, _levelUpO1Rect);
	SpriteBatch::Draw(XPBoostLevelTex, XPBoostLevelPos, XPBoostLevelRect);
	SpriteBatch::Draw(boostTimeTex, boostTimePos, boostTimeRect);
}

void Pacman::deathUpdate()
{
	if (!hasDeathPlayed)
	{
		Audio::Play(deathSound);
		hasDeathPlayed = true;
	}
	Input::KeyboardState* keyboardState = Input::Keyboard::GetState();
	CheckRestart(keyboardState);
}

void Pacman::deathDraw()
{
	SpriteBatch::Draw(_restartScreen, _restartScreenRect, nullptr);
}

void Pacman::ResetLoaded()
{
	hasLevelOneLoaded = false;
	hasLevelTwoLoaded = false;
	XPBoostLevelRect->Y = 0;
	boostTimeRect->Y = 0;
	_levelUpO1Rect->Y = 0;
	hasL1Played = false;
	hasL2Played = false;
	score = 0;
}

void Pacman::controlsUpdate()
{
	Input::KeyboardState* keyboardState = Input::Keyboard::GetState();
	if (keyboardState->IsKeyDown(Input::Keys::ESCAPE))
	{
		gState = mainMenu; 
	}
}

void Pacman::controlsDraw()
{
	SpriteBatch::Draw(controlsScrTex, controlsScrPos, controlsScrRect);
}

void Pacman::nextLevelUpdate()
{
	Input::KeyboardState* keyboardState = Input::Keyboard::GetState();
	if (keyboardState->IsKeyDown(Input::Keys::SPACE))
	{
		if (tempState == level1)
		{
			gState = level2;
			XPBoostLevelRect->Y = 0;
			boostTimeRect->Y = 0;
			_levelUpO1Rect->Y = 0;
		}
		else if (tempState == level2)
		{
			gState = level3;
			XPBoostLevelRect->Y = 0;
			boostTimeRect->Y = 0;
			_levelUpO1Rect->Y = 0;
		}
		else if (tempState == level3)
		{
			gState = level3;
			hasLevelThreeLoaded = false;
			XPBoostLevelRect->Y = 0;
			boostTimeRect->Y = 0;
			_levelUpO1Rect->Y = 0;
		}
		LoadContent();
	}
}

void Pacman::nextLevelDraw()
{
	SpriteBatch::Draw(nextLevelTex, nextLevelPos, nextLevelRect);
}

#pragma region level methods

void Pacman::LevelOneLoad()
{
	ghostsToKill = 4;
	//load collectible
	Texture2D* munchTexture = new Texture2D();
	munchTexture->Load("Textures/scrap.png", true);
	Rect* munchRect = new Rect(0.0f, 0.0f, 16, 16);
	Rect* munchRect2 = new Rect(0, 16, 16, 16);
	Rect* munchRect3 = new Rect(0, 32, 16, 16);
	for (int i = 0; i < MUNCHIECOUNT; i++)
	{
		bool check = true;

		Vector2* position = new Vector2(rand() % 3740, (rand() % Graphics::GetViewportHeight()) - 95);
		int munchrand = rand() % 100;
		if (munchrand < 33)
		{
			munchie[i] = new Collectible(position, munchRect, munchTexture);
		}
		else if (munchrand < 66)
		{
			munchie[i] = new Collectible(position, munchRect2, munchTexture);
		}
		else
		{
			munchie[i] = new Collectible(position, munchRect3, munchTexture);
		}
		int count = 0;
		while (check && count < 500)
		{
			count++;
			for (int j = i - 1; j > 0; j--)
			{
				if (CheckCollisions(*munchie[i], *munchie[j]))
				{
					check = true;
					munchie[i]->setPosition(rand() % 3740, (rand() % Graphics::GetViewportHeight()) - 95);
				}
				else
				{
					check = false;
				}
			}
		}
	}

	_backGround1 = new Texture2D();
	_backGround1->Load("Textures/Backgrounds/space.jpg", true);
	_backGroundRect1 = new Rect(0.0f, 0.0f, Graphics::GetViewportWidth(), Graphics::GetViewportHeight());
	_backGroundPos1 = new Vector2(0.0f, 0.0f);

	//Load pacman
	Texture2D* pacTex = new Texture2D();
	pacTex->Load("Textures/rocket.png", false);
	Texture2D* bulletText = new Texture2D();
	bulletText->Load("Textures/bullet.png", false);
	Vector2* pacPos = new Vector2(350.0f, 300.0f);
	Rect* pacRect = new Rect(0.0f, 0.0f, 32, 32);
	pacman = new Player(pacPos, pacRect, pacTex, bulletText);


	//Load ship data
	Texture2D* shipTex = new Texture2D();
	shipTex->Load("Textures/enemyShip.png", false);
	Texture2D* shipTexSauc = new Texture2D();
	shipTexSauc->Load("Textures/saucer.png", false);
	Rect* shipRect1 = new Rect(0.0f, 0.0f, 29, 29);
	Rect* shipRect2 = new Rect(0.0f, 0.0f, 29, 29);
	Rect* shipRectSauc1 = new Rect(0.0f, 0.0f, 32, 32);
	Rect* shipRectSauc2 = new Rect(0.0f, 0.0f, 32, 32);
	//load chase ships
	Vector2* gbPos = new Vector2(10.0f, 10.0f);
	ship[0] = new Enemy(gbPos, shipRectSauc1, shipTexSauc);
	ship[0]->SetAI(still);
	gbPos = new Vector2(2000.0f, 60.0f);
	ship[1] = new Enemy(gbPos, shipRectSauc2, shipTexSauc);
	ship[1]->SetAI(still);
	//Load patrol ships
	gbPos = new Vector2(400, 200);
	ship[2] = new Enemy(gbPos, shipRect1, shipTex);
	ship[2]->SetAI(patrol);
	gbPos = new Vector2(1300, 400);
	ship[3] = new Enemy(gbPos, shipRect2, shipTex);
	ship[3]->SetAI(patrol);
	//ghosts not in this level
	gbPos = new Vector2(-10000.0f, -10000.0f);
	ship[4] = new Enemy(gbPos, shipRectSauc2, shipTexSauc);
	ship[4]->SetAI(still);
	ship[4]->setHealth(-10);
	gbPos = new Vector2(-10000.0f, -10000.0f);
	ship[5] = new Enemy(gbPos, shipRectSauc2, shipTexSauc);
	ship[5]->SetAI(still);
	ship[5]->setHealth(-10);
	gbPos = new Vector2(-10000.0f, -10000.0f);
	ship[6] = new Enemy(gbPos, shipRectSauc2, shipTexSauc);
	ship[6]->SetAI(still);
	ship[6]->setHealth(-10);



	//Load portal data
	Texture2D* portalTex = new Texture2D();
	portalTex->Load("Textures/Portal.png", false);
	Rect* portalRect = new Rect(0.0f, 0.0f, 32, 32);

	//load portals
	Vector2* portalPos = new Vector2(400, 500);
	portal[0] = new Portal(portalPos, portalRect, portalTex);
	portalPos = new Vector2(2000, 250);
	portal[1] = new Portal(portalPos, portalRect, portalTex);
	portalPos = new Vector2(-1000, -1000);
	portal[2] = new Portal(portalPos, portalRect, portalTex);
	portalPos = new Vector2(-1000, -1000);
	portal[3] = new Portal(portalPos, portalRect, portalTex);

	hasLevelOneLoaded = true;
}

void Pacman::levelOneUpdate(int elapsedTime)
{
	Input::KeyboardState* keyboardState = Input::Keyboard::GetState();
	Input::MouseState* mouseState = Input::Mouse::GetState();


	if (ghostsToKill <= 0)
	{
		tempState = gState;
		gState = nextLevel;
		Audio::Stop(levelOneSong);
	}

	shotTime += elapsedTime / 1000;
	CheckPaused(keyboardState, Input::Keys::P);

	if (pacman->GetXP() >= 100)
	{
		tempState = level1;
		gState = levelUp;
	}


	if (!hasL1Played)
	{
		Audio::Play(levelOneSong);
		hasL1Played = true;
	}

	for (int i = 0; i < pacman->bulletList.size(); i++)
	{
		pacman->bulletList.at(i).update(elapsedTime);
		if (pacman->bulletList.at(i).CheckViewPortColl() == true)
		{
			pacman->deleteBullet(i);
		}
	}
	Input(elapsedTime, keyboardState, mouseState);

	for (int i = 0; i < GHOSTCOUNT; i++)
	{
		ship[i]->UpdateGhosts(elapsedTime, pacman);
	}
	pacman->Update(elapsedTime);


	if (pacman->GetXP() > 100)
	{
		pacman->SetXP(100);
	}

#pragma region Collision checks
	for (Collectible *n : munchie)
	{
		n->UpdateMunchie(elapsedTime);
		if (CheckCollisions(*pacman, *n))
		{
			Audio::Play(_pop);
			n->setPosition(-1000.0, -1000.0);
			score += 100;
			pacman->SetXP(pacman->GetXP() + pacman->GetXPInc());
		}
	}

	for (int j = 0; j < GHOSTCOUNT; j++)
	{
		if (ship[j]->GetAI() == patrol)
		{
			for (int i = 0; i < pacman->bulletList.size(); i++)
			{

				if (CheckCollisions(pacman->bulletList.at(i), *ship[j]))
				{
					ship[j]->DecrHealth();
					pacman->deleteBullet(i);
					if (ship[j]->CheckDead(pacman))
					{
						score += 100;
						ghostsToKill--;
					}
				}
			}
		}
	}

	for (Enemy *n : ship)
	{
		if (n->GetAI() == chase)
		{
			for (Portal *p : portal)
			{
				if (CheckCollisions(*p, *n))
				{
					n->setHealth(0);
					ghostsToKill--;
				}
			}
		}
	}


	for (Enemy *n : ship)
	{
		if (CheckCollisions(*pacman, *n))
		{
			Audio::Stop(levelOneSong);
			gState = deathScr;
		}
	}

	for (Portal *n : portal)
	{
		n->Update(elapsedTime);
		if (CheckCollisions(*pacman, *n))
		{
			Audio::Stop(levelOneSong);
			gState = deathScr;
		}
	}

	pacman->CheckViewportCollision();
	if (pacman->CheckRight())
	{
		if (_backGroundRect1->X < (3840 - Graphics::GetViewportWidth()))
		{
			_backGroundRect1->X += pacman->GetActualSpeed() * elapsedTime;
			for (auto n : munchie)
			{
				n->setPosition(n->getPosition()->X - (pacman->GetActualSpeed() * elapsedTime), n->getPosition()->Y);
			}
			for (auto n : portal)
			{
				n->setPosition(n->getPosition()->X -= (pacman->GetActualSpeed() * elapsedTime), n->getPosition()->Y);
			}
			for (auto n : ship)
			{
				n->setPosition(n->getPosition()->X -= (pacman->GetActualSpeed() * elapsedTime), n->getPosition()->Y);
			}
		}
	}
	if (pacman->CheckLeft())
	{
		if (_backGroundRect1->X > 0)
		{
			_backGroundRect1->X -= pacman->GetActualSpeed() * elapsedTime;
			for (auto n : munchie)
			{
				n->setPosition(n->getPosition()->X + (pacman->GetActualSpeed() * elapsedTime), n->getPosition()->Y);
			}
			for (auto n : portal)
			{
				n->setPosition(n->getPosition()->X += (pacman->GetActualSpeed() * elapsedTime), n->getPosition()->Y);
			}
			for (auto n : ship)
			{
				n->setPosition(n->getPosition()->X += (pacman->GetActualSpeed() * elapsedTime), n->getPosition()->Y);
			}
		}
	}
#pragma endregion



}

void Pacman::levelOneDraw()
{
	// Allows us to easily create a string
	std::stringstream stream;
	stream << "Score: " << score;

	SpriteBatch::BeginDraw(); // Starts Drawing

	//Draws background
	SpriteBatch::Draw(_backGround1, _backGroundPos1, _backGroundRect1, Vector2::Zero, 1.0f, 0.0f, Color::White, SpriteEffect::NONE);

	for (Portal *n : portal)
	{
		SpriteBatch::Draw(n->getTexture(), n->getPosition(), n->getRect());
	}


	for (int i = 0; i < pacman->bulletList.size(); i++)
	{
		SpriteBatch::Draw(pacman->bulletList.at(i).getTexture(), pacman->bulletList.at(i).getPosition(), pacman->bulletList.at(i).getRect());
	}

	//Draw all munchies
	for (Collectible *n : munchie)
	{
		SpriteBatch::Draw(n->getTexture(), n->getPosition(), n->getRect()); //draw munchies
	}

	SpriteBatch::Draw(pacman->getTexture(), pacman->getPosition(), pacman->getRect());

	for (Enemy *n : ship)
	{
		if (!n->CheckDead(pacman))
		{
			SpriteBatch::Draw(n->getTexture(), n->getPosition(), n->getRect());
		}
	}

	//Draws powerUp bar
	SpriteBatch::DrawRectangle(10, 728, 206, 26, S2D::Color::White);
	float width = (pacman->GetMaxBoost() * 400) - (pacman->GetActualBoost() * 390);
	SpriteBatch::DrawRectangle(13, 731, width, 20, S2D::Color::Red);

	// Draws String
	SpriteBatch::DrawString(stream.str().c_str(), _stringPosition, Color::White);


#pragma region Draw xp bar
	//Draw xp bar
	SpriteBatch::DrawRectangle(250, 728, 206, 26, S2D::Color::White);
	SpriteBatch::DrawRectangle(253, 731, pacman->GetXP() * 2, 20, S2D::Color::Red);
	std::stringstream xpBar;
	xpBar << "XP";
	SpriteBatch::DrawString(xpBar.str().c_str(), _xpBarStringPos, Color::Black);
	std::stringstream level;
	level << "Level: " << pacman->GetLevel();
	SpriteBatch::DrawString(level.str().c_str(), _levelStringPos, Color::White);
	std::stringstream remaining;
	remaining << "Enemy Ships Remaining: " << ghostsToKill;
	SpriteBatch::DrawString(remaining.str().c_str(), remainingPos, Color::White);
#pragma endregion

}

void Pacman::levelTwoLoad()
{
	ghostsToKill = 5;

	//load collectible
	Texture2D* munchTexture = new Texture2D();
	munchTexture->Load("Textures/scrap.png", true);
	Rect* munchRect = new Rect(0.0f, 0.0f, 16, 16);
	Rect* munchRect2 = new Rect(0, 16, 16, 16);
	Rect* munchRect3 = new Rect(0, 32, 16, 16);
	for (int i = 0; i < MUNCHIECOUNT; i++)
	{
		bool check = true;

		Vector2* position = new Vector2(rand() % 3740, (rand() % Graphics::GetViewportHeight()) - 95);
		int munchrand = rand() % 100;
		if (munchrand < 33)
		{
			munchie[i] = new Collectible(position, munchRect, munchTexture);
		}
		else if (munchrand < 66)
		{
			munchie[i] = new Collectible(position, munchRect2, munchTexture);
		}
		else
		{
			munchie[i] = new Collectible(position, munchRect3, munchTexture);
		}
		int count = 0;
		while (check && count < 500)
		{
			count++;
			for (int j = i - 1; j > 0; j--)
			{
				if (CheckCollisions(*munchie[i], *munchie[j]))
				{
					check = true;
					munchie[i]->setPosition(rand() % 3740, (rand() % Graphics::GetViewportHeight()) - 95);
				}
				else
				{
					check = false;
				}
			}
		}
	}

	_backGround1 = new Texture2D();
	_backGround1->Load("Textures/Backgrounds/space.jpg", true);
	_backGroundRect1 = new Rect(0.0f, 1000.0f, Graphics::GetViewportWidth(), Graphics::GetViewportHeight());
	_backGroundPos1 = new Vector2(0.0f, 0.0f);

	//Load pacman
	Texture2D* pacTex = new Texture2D();
	pacTex->Load("Textures/rocket.png", false);
	Texture2D* bulletText = new Texture2D();
	bulletText->Load("Textures/bullet.png", false);
	Vector2* pacPos = new Vector2(350.0f, 350.0f);
	Rect* pacRect = new Rect(0.0f, 0.0f, 32, 32);
	pacman = new Player(pacPos, pacRect, pacTex, bulletText);


	//Load ship data
	Texture2D* shipTex = new Texture2D();
	shipTex->Load("Textures/enemyShip.png", false);
	Texture2D* shipTexSauc = new Texture2D();
	shipTexSauc->Load("Textures/saucer.png", false);
	Rect* shipRect1 = new Rect(0.0f, 0.0f, 29, 29);
	Rect* shipRect2 = new Rect(0.0f, 0.0f, 29, 29);
	Rect* shipRectSauc1 = new Rect(0.0f, 0.0f, 32, 32);
	Rect* shipRectSauc2 = new Rect(0.0f, 0.0f, 32, 32);
	Rect* shipRect3 = new Rect(0.0f, 0.0f, 29, 29);

	//load chase ships
	Vector2* gbPos = new Vector2(300.0f, 10.0f);
	ship[0] = new Enemy(gbPos, shipRectSauc1, shipTexSauc);
	ship[0]->SetAI(still);
	gbPos = new Vector2(200.0f, 10.0f);
	ship[1] = new Enemy(gbPos, shipRectSauc2, shipTexSauc);
	ship[1]->SetAI(still);
	//Load patrol ships
	gbPos = new Vector2(200, 200);
	ship[2] = new Enemy(gbPos, shipRect1, shipTex);
	ship[2]->SetAI(patrol);
	gbPos = new Vector2(1600, 400);
	ship[3] = new Enemy(gbPos, shipRect2, shipTex);
	ship[3]->SetAI(patrol);
	gbPos = new Vector2(900.0f, 300.0f);
	ship[4] = new Enemy(gbPos, shipRect3, shipTex);
	ship[4]->SetAI(patrol);
	//ghosts not in this level
	gbPos = new Vector2(-10000.0f, -10000.0f);
	ship[5] = new Enemy(gbPos, shipRectSauc2, shipTexSauc);
	ship[5]->SetAI(still);
	ship[5]->setHealth(-10);
	gbPos = new Vector2(-10000.0f, -10000.0f);
	ship[6] = new Enemy(gbPos, shipRectSauc2, shipTexSauc);
	ship[6]->SetAI(still);
	ship[6]->setHealth(-10);



	//Load portal data
	Texture2D* portalTex = new Texture2D();
	portalTex->Load("Textures/Portal.png", false);
	Rect* portalRect = new Rect(0.0f, 0.0f, 32, 32);

	//load portals
	Vector2* portalPos = new Vector2(700, 400);
	portal[0] = new Portal(portalPos, portalRect, portalTex);
	portalPos = new Vector2(2000, 250);
	portal[1] = new Portal(portalPos, portalRect, portalTex);
	portalPos = new Vector2(1000, 600);
	portal[2] = new Portal(portalPos, portalRect, portalTex);
	portalPos = new Vector2(-1000, -1000);
	portal[3] = new Portal(portalPos, portalRect, portalTex);

	hasLevelTwoLoaded = true;
}

void Pacman::levelTwoUpdate(int elapsedTime)
{
	Input::KeyboardState* keyboardState = Input::Keyboard::GetState();
	Input::MouseState* mouseState = Input::Mouse::GetState();


	if (ghostsToKill <= 0)
	{
		tempState = gState;
		gState = nextLevel;
	}

	shotTime += elapsedTime / 1000;
	CheckPaused(keyboardState, Input::Keys::P);

	if (pacman->GetXP() >= 100)
	{
		tempState = level2;
		gState = levelUp;
	}


	if (!hasL2Played)
	{
		Audio::Play(levelTwoSong);
		hasL2Played = true;
	}

	for (int i = 0; i < pacman->bulletList.size(); i++)
	{
		pacman->bulletList.at(i).update(elapsedTime);
		if (pacman->bulletList.at(i).CheckViewPortColl() == true)
		{
			pacman->deleteBullet(i);
		}
	}
	Input(elapsedTime, keyboardState, mouseState);

	for (int i = 0; i < GHOSTCOUNT; i++)
	{
		ship[i]->UpdateGhosts(elapsedTime, pacman);
	}
	pacman->Update(elapsedTime);


	if (pacman->GetXP() > 100)
	{
		pacman->SetXP(100);
	}

#pragma region Collision checks
	for (Collectible *n : munchie)
	{
		n->UpdateMunchie(elapsedTime);
		if (CheckCollisions(*pacman, *n))
		{
			Audio::Play(_pop);
			n->setPosition(-1000.0, -1000.0);
			score += 100;
			pacman->SetXP(pacman->GetXP() + pacman->GetXPInc());
		}
	}

	for (int j = 0; j < GHOSTCOUNT; j++)
	{
		if (ship[j]->GetAI() == patrol)
		{
			for (int i = 0; i < pacman->bulletList.size(); i++)
			{

				if (CheckCollisions(pacman->bulletList.at(i), *ship[j]))
				{
					ship[j]->DecrHealth();
					pacman->deleteBullet(i);
					if (ship[j]->CheckDead(pacman))
					{
						score += 100;
						ghostsToKill--;
					}
				}
			}
		}
	}

	for (Enemy *n : ship)
	{
		if (n->GetAI() == chase)
		{
			for (Portal *p : portal)
			{
				if (CheckCollisions(*p, *n))
				{
					n->setHealth(0);
					ghostsToKill--;
				}
			}
		}
	}


	for (Enemy *n : ship)
	{
		if (CheckCollisions(*pacman, *n))
		{
			Audio::Stop(levelTwoSong);
			hasL1Played = false;
			gState = deathScr;
		}
	}

	for (Portal *n : portal)
	{
		n->Update(elapsedTime);
		if (CheckCollisions(*pacman, *n))
		{
			Audio::Stop(levelTwoSong);
			hasL1Played = false;
			gState = deathScr;
		}
	}

	pacman->CheckViewportCollision();
	if (pacman->CheckRight())
	{
		if (_backGroundRect1->X < (3840 - Graphics::GetViewportWidth()))
		{
			_backGroundRect1->X += pacman->GetActualSpeed() * elapsedTime;
			for (auto n : munchie)
			{
				n->setPosition(n->getPosition()->X - (pacman->GetActualSpeed() * elapsedTime), n->getPosition()->Y);
			}
			for (auto n : portal)
			{
				n->setPosition(n->getPosition()->X -= (pacman->GetActualSpeed() * elapsedTime), n->getPosition()->Y);
			}
			for (auto n : ship)
			{
				n->setPosition(n->getPosition()->X -= (pacman->GetActualSpeed() * elapsedTime), n->getPosition()->Y);
			}
		}
	}
	if (pacman->CheckLeft())
	{
		if (_backGroundRect1->X > 0)
		{
			_backGroundRect1->X -= pacman->GetActualSpeed() * elapsedTime;
			for (auto n : munchie)
			{
				n->setPosition(n->getPosition()->X + (pacman->GetActualSpeed() * elapsedTime), n->getPosition()->Y);
			}
			for (auto n : portal)
			{
				n->setPosition(n->getPosition()->X += (pacman->GetActualSpeed() * elapsedTime), n->getPosition()->Y);
			}
			for (auto n : ship)
			{
				n->setPosition(n->getPosition()->X += (pacman->GetActualSpeed() * elapsedTime), n->getPosition()->Y);
			}
		}
	}
#pragma endregion
}

void Pacman::levelTwoDraw()
{
	// Allows us to easily create a string
	std::stringstream stream;
	stream << "Score: " << score;

	SpriteBatch::BeginDraw(); // Starts Drawing

	//Draws background
	SpriteBatch::Draw(_backGround1, _backGroundPos1, _backGroundRect1, Vector2::Zero, 1.0f, 0.0f, Color::White, SpriteEffect::NONE);

	for (Portal *n : portal)
	{
		SpriteBatch::Draw(n->getTexture(), n->getPosition(), n->getRect());
	}


	for (int i = 0; i < pacman->bulletList.size(); i++)
	{
		SpriteBatch::Draw(pacman->bulletList.at(i).getTexture(), pacman->bulletList.at(i).getPosition(), pacman->bulletList.at(i).getRect());
	}

	//Draw all munchies
	for (Collectible *n : munchie)
	{
		SpriteBatch::Draw(n->getTexture(), n->getPosition(), n->getRect()); //draw munchies
	}

	SpriteBatch::Draw(pacman->getTexture(), pacman->getPosition(), pacman->getRect());

	for (Enemy *n : ship)
	{
		if (!n->CheckDead(pacman))
		{
			SpriteBatch::Draw(n->getTexture(), n->getPosition(), n->getRect());
		}
	}

	//Draws powerUp bar
	SpriteBatch::DrawRectangle(10, 728, 206, 26, S2D::Color::White);
	float width = (pacman->GetMaxBoost() * 400) - (pacman->GetActualBoost() * 390);
	SpriteBatch::DrawRectangle(13, 731, width, 20, S2D::Color::Red);

	// Draws String
	SpriteBatch::DrawString(stream.str().c_str(), _stringPosition, Color::White);


#pragma region Draw xp bar
	//Draw xp bar
	SpriteBatch::DrawRectangle(250, 728, 206, 26, S2D::Color::White);
	SpriteBatch::DrawRectangle(253, 731, pacman->GetXP() * 2, 20, S2D::Color::Red);
	std::stringstream xpBar;
	xpBar << "XP";
	SpriteBatch::DrawString(xpBar.str().c_str(), _xpBarStringPos, Color::Black);
	std::stringstream level;
	level << "Level: " << pacman->GetLevel();
	SpriteBatch::DrawString(level.str().c_str(), _levelStringPos, Color::White);
	std::stringstream remaining;
	remaining << "Enemy Ships Remaining: " << ghostsToKill;
	SpriteBatch::DrawString(remaining.str().c_str(), remainingPos, Color::White);
#pragma endregion
}

void Pacman::levelThreeLoad()
{
	ghostsToKill = 7;

	//load collectible
	Texture2D* munchTexture = new Texture2D();
	munchTexture->Load("Textures/scrap.png", true);
	Rect* munchRect = new Rect(0.0f, 0.0f, 16, 16);
	Rect* munchRect2 = new Rect(0, 16, 16, 16);
	Rect* munchRect3 = new Rect(0, 32, 16, 16);
	for (int i = 0; i < MUNCHIECOUNT; i++)
	{
		bool check = true;

		Vector2* position = new Vector2((rand() % 3690) + 151, (rand() % Graphics::GetViewportHeight()) - 95);
		int munchrand = rand() % 100;
		if (munchrand < 33)
		{
			munchie[i] = new Collectible(position, munchRect, munchTexture);
		}
		else if (munchrand < 66)
		{
			munchie[i] = new Collectible(position, munchRect2, munchTexture);
		}
		else
		{
			munchie[i] = new Collectible(position, munchRect3, munchTexture);
		}
		int count = 0;
		while (check && count < 500)
		{
			count++;
			for (int j = i - 1; j > 0; j--)
			{
				if (CheckCollisions(*munchie[i], *munchie[j]))
				{
					check = true;
					munchie[i]->setPosition((rand() % 3690) + 151, (rand() % Graphics::GetViewportHeight()) - 95);
				}
				else
				{
					check = false;
				}
			}
		}
	}

	_backGround1 = new Texture2D();
	_backGround1->Load("Textures/Backgrounds/space.jpg", true);
	_backGroundRect1 = new Rect(0.0f, 1000.0f, Graphics::GetViewportWidth(), Graphics::GetViewportHeight());
	_backGroundPos1 = new Vector2(0.0f, 0.0f);

	//Load pacman
	Texture2D* pacTex = new Texture2D();
	pacTex->Load("Textures/rocket.png", false);
	Texture2D* bulletText = new Texture2D();
	bulletText->Load("Textures/bullet.png", false);
	Vector2* pacPos = new Vector2(350.0f, 350.0f);
	Rect* pacRect = new Rect(0.0f, 0.0f, 32, 32);
	pacman = new Player(pacPos, pacRect, pacTex, bulletText);


	//Load ship data
	Texture2D* shipTex = new Texture2D();
	shipTex->Load("Textures/enemyShip.png", false);
	Texture2D* shipTexSauc = new Texture2D();
	shipTexSauc->Load("Textures/saucer.png", false);
	Rect* shipRect1 = new Rect(0.0f, 0.0f, 29, 29);
	Rect* shipRect2 = new Rect(0.0f, 0.0f, 29, 29);
	Rect* shipRect3 = new Rect(0.0f, 0.0f, 29, 29);
	Rect* shipRect4 = new Rect(0.0f, 0.0f, 29, 29);
	Rect* shipRectSauc1 = new Rect(0.0f, 0.0f, 32, 32);
	Rect* shipRectSauc2 = new Rect(0.0f, 0.0f, 32, 32);
	Rect* shipRectSauc3 = new Rect(0.0f, 0.0f, 32, 32);

	//load chase ships
	Vector2* gbPos = new Vector2((rand() % 3690) + 151, (rand() % Graphics::GetViewportHeight()) - 95);
	ship[0] = new Enemy(gbPos, shipRectSauc1, shipTexSauc);
	ship[0]->SetAI(still);
	gbPos = new Vector2((rand() % 3690) + 151, (rand() % Graphics::GetViewportHeight()) - 95);
	ship[1] = new Enemy(gbPos, shipRectSauc2, shipTexSauc);
	ship[1]->SetAI(still);
	//Load patrol ships
	gbPos = new Vector2((rand() % 3690) + 151, (rand() % Graphics::GetViewportHeight()) - 95);
	ship[2] = new Enemy(gbPos, shipRect1, shipTex);
	ship[2]->SetAI(patrol);
	gbPos = new Vector2((rand() % 3690) + 151, (rand() % Graphics::GetViewportHeight()) - 95);
	ship[3] = new Enemy(gbPos, shipRect2, shipTex);
	ship[3]->SetAI(patrol);
	gbPos = new Vector2((rand() % 3690) + 151, (rand() % Graphics::GetViewportHeight()) - 95);
	ship[4] = new Enemy(gbPos, shipRect3, shipTex);
	ship[4]->SetAI(patrol);
	//ghosts not in this level
	gbPos = new Vector2((rand() % 3690) + 151, (rand() % Graphics::GetViewportHeight()) - 95);
	ship[5] = new Enemy(gbPos, shipRectSauc3, shipTexSauc);
	ship[5]->SetAI(still);
	gbPos = new Vector2((rand() % 3690) + 151, (rand() % Graphics::GetViewportHeight()) - 95);
	ship[6] = new Enemy(gbPos, shipRect4, shipTexSauc);
	ship[6]->SetAI(patrol);



	//Load portal data
	Texture2D* portalTex = new Texture2D();
	portalTex->Load("Textures/Portal.png", false);
	Rect* portalRect = new Rect(0.0f, 0.0f, 32, 32);

	//load portals
	Vector2* portalPos = new Vector2((rand() % 3740) - 100, (rand() % Graphics::GetViewportHeight()) - 95);
	portal[0] = new Portal(portalPos, portalRect, portalTex);
	portalPos = new Vector2((rand() % 3740) - 100, (rand() % Graphics::GetViewportHeight()) - 95);
	portal[1] = new Portal(portalPos, portalRect, portalTex);
	portalPos = new Vector2((rand() % 3740) - 100, (rand() % Graphics::GetViewportHeight()) - 95);
	portal[2] = new Portal(portalPos, portalRect, portalTex);
	portalPos = new Vector2((rand() % 3740) - 100, (rand() % Graphics::GetViewportHeight()) - 95);
	portal[3] = new Portal(portalPos, portalRect, portalTex);

	hasLevelThreeLoaded = true;
}

void Pacman::levelThreeUpdate(int elapsedTime)
{
	Input::KeyboardState* keyboardState = Input::Keyboard::GetState();
	Input::MouseState* mouseState = Input::Mouse::GetState();


	if (ghostsToKill <= 0)
	{
		tempState = gState;
		gState = nextLevel;
	}

	shotTime += elapsedTime / 1000;
	CheckPaused(keyboardState, Input::Keys::P);

	if (pacman->GetXP() >= 100)
	{
		tempState = level3;
		gState = levelUp;
	}


	if (!hasL2Played)
	{
		Audio::Play(levelTwoSong);
		hasL2Played = true;
	}

	for (int i = 0; i < pacman->bulletList.size(); i++)
	{
		pacman->bulletList.at(i).update(elapsedTime);
		if (pacman->bulletList.at(i).CheckViewPortColl() == true)
		{
			pacman->deleteBullet(i);
		}
	}
	Input(elapsedTime, keyboardState, mouseState);

	for (int i = 0; i < GHOSTCOUNT; i++)
	{
		ship[i]->UpdateGhosts(elapsedTime, pacman);
	}
	pacman->Update(elapsedTime);


	if (pacman->GetXP() > 100)
	{
		pacman->SetXP(100);
	}

#pragma region Collision checks
	for (Collectible *n : munchie)
	{
		n->UpdateMunchie(elapsedTime);
		if (CheckCollisions(*pacman, *n))
		{
			Audio::Play(_pop);
			n->setPosition(-1000.0, -1000.0);
			score += 100;
			pacman->SetXP(pacman->GetXP() + pacman->GetXPInc());
		}
	}

	for (int j = 0; j < GHOSTCOUNT; j++)
	{
		if (ship[j]->GetAI() == patrol)
		{
			for (int i = 0; i < pacman->bulletList.size(); i++)
			{

				if (CheckCollisions(pacman->bulletList.at(i), *ship[j]))
				{
					ship[j]->DecrHealth();
					pacman->deleteBullet(i);
					if (ship[j]->CheckDead(pacman))
					{
						score += 100;
						ghostsToKill--;
					}
				}
			}
		}
	}

	for (Enemy *n : ship)
	{
		if (n->GetAI() == chase)
		{
			for (Portal *p : portal)
			{
				if (CheckCollisions(*p, *n))
				{
					n->setHealth(0);
					ghostsToKill--;
				}
			}
		}
	}


	for (Enemy *n : ship)
	{
		if (CheckCollisions(*pacman, *n))
		{
			Audio::Stop(levelTwoSong);
			hasL1Played = false;
			gState = deathScr;
		}
	}

	for (Portal *n : portal)
	{
		n->Update(elapsedTime);
		if (CheckCollisions(*pacman, *n))
		{
			Audio::Stop(levelTwoSong);
			hasL1Played = false;
			gState = deathScr;
		}
	}

	pacman->CheckViewportCollision();
	if (pacman->CheckRight())
	{
		if (_backGroundRect1->X < (3840 - Graphics::GetViewportWidth()))
		{
			_backGroundRect1->X += pacman->GetActualSpeed() * elapsedTime;
			for (auto n : munchie)
			{
				n->setPosition(n->getPosition()->X - (pacman->GetActualSpeed() * elapsedTime), n->getPosition()->Y);
			}
			for (auto n : portal)
			{
				n->setPosition(n->getPosition()->X -= (pacman->GetActualSpeed() * elapsedTime), n->getPosition()->Y);
			}
			for (auto n : ship)
			{
				n->setPosition(n->getPosition()->X -= (pacman->GetActualSpeed() * elapsedTime), n->getPosition()->Y);
			}
		}
	}
	if (pacman->CheckLeft())
	{
		if (_backGroundRect1->X > 0)
		{
			_backGroundRect1->X -= pacman->GetActualSpeed() * elapsedTime;
			for (auto n : munchie)
			{
				n->setPosition(n->getPosition()->X + (pacman->GetActualSpeed() * elapsedTime), n->getPosition()->Y);
			}
			for (auto n : portal)
			{
				n->setPosition(n->getPosition()->X += (pacman->GetActualSpeed() * elapsedTime), n->getPosition()->Y);
			}
			for (auto n : ship)
			{
				n->setPosition(n->getPosition()->X += (pacman->GetActualSpeed() * elapsedTime), n->getPosition()->Y);
			}
		}
	}
#pragma endregion
}

void Pacman::levelThreeDraw()
{
	// Allows us to easily create a string
	std::stringstream stream;
	stream << "Score: " << score;

	SpriteBatch::BeginDraw(); // Starts Drawing

	//Draws background
	SpriteBatch::Draw(_backGround1, _backGroundPos1, _backGroundRect1, Vector2::Zero, 1.0f, 0.0f, Color::White, SpriteEffect::NONE);

	for (Portal *n : portal)
	{
		SpriteBatch::Draw(n->getTexture(), n->getPosition(), n->getRect());
	}


	for (int i = 0; i < pacman->bulletList.size(); i++)
	{
		SpriteBatch::Draw(pacman->bulletList.at(i).getTexture(), pacman->bulletList.at(i).getPosition(), pacman->bulletList.at(i).getRect());
	}

	//Draw all munchies
	for (Collectible *n : munchie)
	{
		SpriteBatch::Draw(n->getTexture(), n->getPosition(), n->getRect()); //draw munchies
	}

	SpriteBatch::Draw(pacman->getTexture(), pacman->getPosition(), pacman->getRect());

	for (Enemy *n : ship)
	{
		if (!n->CheckDead(pacman))
		{
			SpriteBatch::Draw(n->getTexture(), n->getPosition(), n->getRect());
		}
	}

	//Draws powerUp bar
	SpriteBatch::DrawRectangle(10, 728, 206, 26, S2D::Color::White);
	float width = (pacman->GetMaxBoost() * 400) - (pacman->GetActualBoost() * 390);
	SpriteBatch::DrawRectangle(13, 731, width, 20, S2D::Color::Red);

	// Draws String
	SpriteBatch::DrawString(stream.str().c_str(), _stringPosition, Color::White);


#pragma region Draw xp bar
	//Draw xp bar
	SpriteBatch::DrawRectangle(250, 728, 206, 26, S2D::Color::White);
	SpriteBatch::DrawRectangle(253, 731, pacman->GetXP() * 2, 20, S2D::Color::Red);
	std::stringstream xpBar;
	xpBar << "XP";
	SpriteBatch::DrawString(xpBar.str().c_str(), _xpBarStringPos, Color::Black);
	std::stringstream level;
	level << "Level: " << pacman->GetLevel();
	SpriteBatch::DrawString(level.str().c_str(), _levelStringPos, Color::White);
	std::stringstream remaining;
	remaining << "Enemy Ships Remaining: " << ghostsToKill;
	SpriteBatch::DrawString(remaining.str().c_str(), remainingPos, Color::White);
#pragma endregion
}

#pragma endregion
