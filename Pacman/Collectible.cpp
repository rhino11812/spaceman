#include "Collectible.h"


Collectible::Collectible(Vector2* pos, Rect* rect, Texture2D* tex) : Collidable(pos, rect, tex)
{
	currentFrametime = 0;
	frame = 0;
	frameTime = 500;
}


Collectible::~Collectible()
{
	delete texture;
	delete position;
	delete sourceRect;
}

void Collectible::UpdateMunchie(int elapsedTime)
{
	if (currentFrametime > frameTime)
	{
		frame++;
		if (frame >= 2)
			frame = 0;

		currentFrametime = 0;
	}
	sourceRect->X = sourceRect->Width * frame;
	currentFrametime += elapsedTime;
}
