#include "Collidable.h"


Collidable::Collidable(Vector2* pos, Rect* rect, Texture2D* tex)
{
	position = pos;
	sourceRect = rect;
	texture = tex;
}


Collidable::~Collidable()
{

}

Vector2* Collidable::getPosition()
{
	return position;
}
void Collidable::setPosition(float x, float y)
{
	position = new Vector2(x, y);
}

Rect* Collidable::getRect()
{
	return sourceRect;
}

Texture2D* Collidable::getTexture()
{
	return texture;
}
