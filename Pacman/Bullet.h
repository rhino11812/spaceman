#pragma once
#include "Collidable.h"

class Bullet : public Collidable
{
public:
	~Bullet();
	Bullet(Vector2* pos, Rect* rect, Texture2D* tex, Vector2* targetPosi, Vector2* startPos);

	bool CheckViewPortColl();
	void update(int elapsedTime);
	void deletePoint();
	bool deathTime();


private:
	int mouseX;
	int mouseY;
	float currentTime;
	float maxTime;
	
	bool shooting;

	float yDistance;
	float xDistance;

	Vector2* startPosition;
	Vector2* targetPos;
};

