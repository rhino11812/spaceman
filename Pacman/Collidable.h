#pragma once
#include "S2D/S2D.h"
using namespace S2D;

class Collidable
{
public:
	Collidable(Vector2* position, Rect* sourceRect, Texture2D* texture);
	~Collidable();

	virtual Vector2* getPosition();
	virtual void setPosition(float x, float y);
	virtual Rect* getRect();
	virtual Texture2D* getTexture();

protected:
	Vector2* position;
	Rect* sourceRect;
	Texture2D* texture;
};

