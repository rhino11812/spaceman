#pragma once
#include "Collidable.h"
class Portal : public Collidable
{
public:
	Portal(Vector2* pos, Rect* rect, Texture2D* tex);
	~Portal();

	void Update(int ElapsedTime);

private: 
	int frameTime;
	int currentFrametime;
	int frame;

};

