#include "Player.h"



Player::Player(Vector2* pos, Rect* rect, Texture2D* tex, Texture2D* bulletText) : Collidable(pos, rect, tex)
{
	//Data for pacman
	currentFrameTime = 0;
	frame = 0;
	direction = 4;
	frametime = 250;
	speed = 0.1;
	actualSpeed = 0.1;
	tempSpeed = actualSpeed * 4;
	dead = false;

	isBoosting = false;
	isShiftKeyDown = false;
	boostTime = 0.0f;
	maxBoostTime = 0.5f;

	xp = 0;
	xpInc = 10;
	score = 0;
	level = 1;
	speedLevel = 0;
	XPBoostLevel = 0;
	boostTimeLevel = 0;

	wDown = false;
	aDown = false;
	sDown = false;
	dDown = false;

	stopSpeedRight = false;
	stopSpeedRight = false;

	boostTimeSpeed = 3000;
	
	vector<Bullet> bulletList;
	bulletTex = bulletText;

	_gun = new SoundEffect();
	_gun->Load("Sounds/gunSound.wav");
	_collision = new SoundEffect(); 
	_collision->Load("Sounds/hitSound.wav");

	shotTime = 0;
	maxShotTime = 0.4;

}

Player::~Player()
{
	delete texture;
	delete position;
	delete sourceRect;
	delete bulletTex;
}

void Player::Update(int elapsedTime)
{
		shotTime += (float)elapsedTime / 1000;
		// Checks for the direction for pacman to move
		if (direction == 1 && !stopSpeedRight && !stopSpeedLeft)
		{
			position->Y += actualSpeed * elapsedTime; //Moves Pacman across Y axis
		}
	
		else if (direction == 2 && !stopSpeedLeft)
		{
			position->X -= actualSpeed * elapsedTime; //Moves Pacman across X axis
		}
		else if (direction == 3 && !stopSpeedRight && !stopSpeedLeft)
		{
			position->Y -= actualSpeed * elapsedTime; //Moves Pacman across Y axis
		}
		else if (direction == 4 && !stopSpeedRight)
		{
			position->X += actualSpeed * elapsedTime; //Moves Pacman across X axis
	
		}
	
		sourceRect->Y = sourceRect->Height * direction;
	
	
		if (currentFrameTime > frametime && frame != 2)
		{
			frame++;
			currentFrameTime = 0;
		}
		sourceRect->X = sourceRect->Width * frame;
		
		if (isBoosting)
		{
			currentFrameTime += (elapsedTime * 2);
		}
		else if (!isBoosting)
		{
			currentFrameTime += elapsedTime;
		}
}

void Player::CheckPacmanInput(int elapsedTime, Input::KeyboardState* state, Input::MouseState* mState)
{
	 //Checks if D key is pressed
	if (state->IsKeyDown(Input::Keys::D) && !dDown && direction != 4)
		{
			direction = 4;
			frame = 0;
			dDown = true;
			currentFrameTime = 0;
		}
		// Checks if A key is pressed
		else if (state->IsKeyDown(Input::Keys::A) && !aDown && direction != 2)
		{
			direction = 2;
			frame = 0;
			aDown = true;
			currentFrameTime = 0;

		}
		// Checks if W key is pressed
		else if (state->IsKeyDown(Input::Keys::W) && !wDown && direction != 3)
		{
			direction = 3;
			frame = 0;
			wDown = true;
			currentFrameTime = 0;
		}
		// Checks if S key is pressed
		else if (state->IsKeyDown(Input::Keys::S) && !sDown && direction != 1)
		{
			direction = 1;
		frame = 0;
			sDown = true;
			currentFrameTime = 0;
		}
		if (state->IsKeyUp(Input::Keys::W))
		{
			wDown = false;
		}
		if (state->IsKeyUp(Input::Keys::A))
		{
			aDown = false;
		}
		if (state->IsKeyUp(Input::Keys::S))
		{
			sDown = false;
		}
		if (state->IsKeyUp(Input::Keys::D))
		{
			dDown = false;
		}

		
		//Boost - checks if leftshift is down AND if boost time is lower than the max boost time
		if (state->IsKeyDown(Input::Keys::LEFTSHIFT) && (boostTime < maxBoostTime))
		{
			actualSpeed = tempSpeed;
			boostTime += (float)elapsedTime / 1000;
			isBoosting = true;
		}
		// if leftshift is up or time goes over max, stop boosting
		else if (state->IsKeyUp(Input::Keys::LEFTSHIFT) || boostTime >= maxBoostTime)
		{
			actualSpeed = speed;
			isBoosting = false;	
		}
		//if leftshift is up, reset boost time
		if (state->IsKeyUp(Input::Keys::LEFTSHIFT))
		{
			if (boostTime >= 0)
			{
				boostTime -= (float)elapsedTime / boostTimeSpeed;
			}
			else 
			{
				boostTime = 0;
			}

			isBoosting = false;
		}

		if (mState->LeftButton == Input::ButtonState::PRESSED && shotTime >= maxShotTime)
		{
			Audio::Play(_gun);
			shoot(mState);
			shotTime = 0;
		}
}
void Player::CheckViewportCollision()
{
	if (position->Y > (Graphics::GetViewportHeight() - sourceRect->Height) - 65)
	{
		direction = 3;
	}
	if (position->Y < 0())
	{
		direction = 1;
	}	
}

bool Player::CheckRight()
{
	if (position->X > Graphics::GetViewportWidth() - sourceRect->Width - 150) 
	{
		stopSpeedRight = true;
		
		return true;
	}
	stopSpeedRight = false;
	return false;
}

bool Player::CheckLeft()
{
	if (position->X <= 150)
	{
		stopSpeedLeft = true;

		return true;
	}
	stopSpeedLeft = false;
	return false;
}

float Player::GetActualBoost()
{
	return boostTime;
}

float Player::GetMaxBoost()
{
	return maxBoostTime;
}

int Player::GetXP()
{
	return xp;
}

void Player::SetXP(int xpInc)
{
	xp = xpInc;
}

float Player::GetActualSpeed()
{
	return actualSpeed;
}

float Player::GetSpeed()
{
	return speed;
}

float Player::GetTempSpeed()
{
	return tempSpeed;
}

void Player::SetActualSpeed(float x)
{
	actualSpeed = x;
}

void Player::SetSpeed(float x)
{
	speed = x;
}

void Player::SetTempSpeed(float x)
{
	tempSpeed = x;
}

void Player::SetDead(bool x)
{
	dead = x;
}

bool Player::GetDead()
{
	return dead;
}

int Player::GetXPInc()
{
	return xpInc;
}

void Player::SetXPInc(int x)
{
	xpInc = x;
}

int Player::GetLevel()
{
	return level;
}

void Player::SetLevel(int x)
{
	level = x;
}

void Player::shoot(Input::MouseState*state)
{

		Vector2* targetPos = new Vector2(state->X, state->Y);
		Vector2* bulletPos = new Vector2(this->getPosition()->X + (this->getRect()->Width / 2), this->getPosition()->Y + (this->getRect()->Height / 2));
		Rect* bulletRect = new Rect(0.0f, 0.0f, 5, 5);
		Bullet bullet = Bullet(bulletPos, bulletRect, bulletTex, targetPos, this->getPosition());
		bulletList.push_back(bullet);
}

void Player::deleteBullet(int i)
{
	Audio::Play(_collision);
	bulletList.erase(bulletList.begin() + i);
}

void Player::setSpeedLevel(int x)
{
	speedLevel = x;
}

int Player::getSpeedLevel()
{
	return speedLevel;
}

void Player::setXPBoostLevel(int x)
{
	XPBoostLevel = x;
}

int Player::getXPBoostLevel()
{
	return XPBoostLevel;
}

void Player::setBoostTimeLevel(int x)
{
	boostTimeLevel = x;
}

int Player::getBoostTimeLevel()
{
	return boostTimeLevel;
}

int Player::getBoostTime()
{
	return boostTime;
}

void Player::setBoostTime(int x)
{
	boostTime = x;
}