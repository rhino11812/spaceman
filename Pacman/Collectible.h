#pragma once
#include "S2D/S2D.h"
#include "Collidable.h"

using namespace S2D;

class Collectible : public Collidable
{
public:
	Collectible(Vector2* pos, Rect* rect, Texture2D* tex);
	~Collectible();

	void UpdateMunchie(int elapsedTime);

private:
	int frameTime;
	int currentFrametime;
	int frame;
};