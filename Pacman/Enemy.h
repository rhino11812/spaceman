#pragma once
#include "Collidable.h"
#include "Player.h"

enum enemyAI
{
	patrol,
	chase,
	wander,
	still
};

class Enemy : public Collidable
{
public:
	Enemy(Vector2* pos, Rect* rect, Texture2D* tex);
	~Enemy();
	void UpdateGhosts(int elapsedTime, Player* pacman);
	void SetAI(enemyAI ghostAI);
	enemyAI GetAI();
	void DecrHealth();
	void setHealth(int x);
	int GetHealth();
	bool CheckDead(Player* pacman);

private: 
	float speed;
	int direction;
	int health;
	bool dead;

	int frameTime;
	int currentFrametime;
	int frame;


	enemyAI AI;

};

